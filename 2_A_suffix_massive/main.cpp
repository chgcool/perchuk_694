//
// Perchuk Svyatoslav
//

/* Задача 1. Количество различных подстрок. (7 баллов)
A. Дана строка длины n. Найти количество ее различных подстрок.
Используйте суффиксный массив.
Построение суффиксного массива выполняйте за O(n log n).
Вычисление количества различных подстрок выполняйте за O(n). */

#include <fstream>
#include <iostream>
#include <set>
#include <vector>

const char BLANK  = char(0);
const int alphabet_size = 256;

int mod(int param, int n) {
    return (n + param) % n;
}

std::vector<int> buildSuffixArray(const std::string &str) {
    //получим суффиксный массив из отсортированных циклических сдвигов
    std::string s = str + BLANK;
    int n = s.size();
    std::vector<int> suf; //суффиксный массив
    std::vector<int> equiv_classes (n); //массив классов эквивалентности подстрок

    //сортируем подсчетом подстроки длины 1
    std::vector<std::vector<int>> counter (alphabet_size);
    for (int i = 0; i < n; ++i) {
        counter[s[i]].push_back(i);
	}
    for (int i = 0; i < alphabet_size; ++i) {
        for (auto index : counter[i]) {
            suf.push_back(index);
        }
	}

    //строим массив классов эквивалентности
    int class_num = 0;
    for (int i = 0; i < n - 1; ++i) {
        equiv_classes[suf[i]] = class_num;
        if (s[suf[i]] < s[suf[i + 1]]) {
            ++class_num;
        }
    }
    equiv_classes[suf[n - 1]] = class_num;

    // на i-м шаге сортируем циклические подстроки длины 2^i как пары чисел
    // (equiv_classes[j], equiv_classes[j + 2^(i - 1)])
    for (int i = 1; (1 << (i - 1)) < n; ++i) {
        //сортируем по второму элементу
        for (int j = 0; j < n; ++j) {
            suf[j] = mod(suf[j] - (1 << (i - 1)), n);
		}

        //сортируем подсчетом по первому элементу
        std::set<int> classes;
        for (auto num : equiv_classes) {
            classes.insert(num);
        }
        counter.clear();
        counter.resize(classes.size());
        for (int j = 0; j < n; ++j) {
            counter[equiv_classes[suf[j]]].push_back(suf[j]);
        }
        suf.clear();
        for (auto pocket : counter) {
            for (auto new_val : pocket) {
                suf.push_back(new_val);
            }
        }

        //строим новые классы эквивалентности по новому массиву suf[]
        std::vector<int> new_equiv_classes ((size_t)n);
        class_num = 0;
        for (int j = 0; j < n - 1; ++j) {
            new_equiv_classes[suf[j]] = class_num;
            if ((equiv_classes[suf[j]] != equiv_classes[suf[j + 1]]) ||
                (equiv_classes[mod(suf[j] + (1 << (i - 1)), n)] != equiv_classes[mod(suf[j + 1] + (1 << (i - 1)), n)]))
            {
                ++class_num;
            }
        }
        new_equiv_classes[suf[n - 1]] = class_num;
        equiv_classes = new_equiv_classes;
    }
    //не забываем удалить подстроку, начинающуюся с элемента BLANK
    suf.erase(suf.begin());
    return suf;
}

std::vector<int> buildLCP(const std::vector<int> &suf, const std::string &s) {
    //массив, обратный суффиксному
    std::vector<int> inverse(suf.size(), 0);
    for (int i = 0; i < suf.size(); ++i) {
        inverse[suf[i]] = i;
    }
    std::vector<int> lcp(s.size());
    for (int i = 0; i < s.size(); ++i) {
        int q = inverse[i];
        //по теореме lcp[q] >= lcp[inverse[i - 1]] - 1
        //не забудем обойти 2 случая: i == 0 и q == 0
        if (q == 0) {
            continue;
        }
        if (i != 0) {
            lcp[q] = std::max(lcp[inverse[i - 1]] - 1, 0);
        }
        //далее доходим до максимального значения lcp[q] перебором
        int border = s.size() - std::max(suf[q], suf[q - 1]);
        while ((lcp[q] < border) && (s[suf[q] + lcp[q]] == s[suf[q - 1] + lcp[q]])) {
            ++lcp[q];
        }
    }
    lcp.erase(lcp.begin());
    return lcp;
}

int numberOfSubstrings(const std::string& s) {
    auto suf = buildSuffixArray(s);
    auto lcp = buildLCP(suf, s);
    int unique_substrings_count = 0;
    //каждый префикс произвольного суффикса является подстрокой в s, значит, ответом будет сумма количества
    //их префиксов за вычетом количества общих префиксов у соседних (в suf[]) суффиксов.
    for (int i = 0; i < s.size(); ++i) {
        unique_substrings_count += (s.size() - suf[i]);
    }
    for (int i = 0; i < s.size() - 1; ++i) {
        unique_substrings_count -= lcp[i];
    }
    return unique_substrings_count;
}

int main() {
    std::ifstream fin("input.txt");
    std::string s;
    fin >> s;
    std::cout << numberOfSubstrings(s);
    return 0;
}
