//
// Created by Svyatoslav on 24.11.2017.
//

#include <cmath>
#include <deque>
#include <fstream>
#include <iostream>
#include <vector>

template<typename T>
void print(const T &v) {
    for (auto p : v) {
        std::cout << '(' << p.x << ',' << p.y << ')';
    }
    std::cout << '\n';
}

struct Point {
    double x;
    double y;

    Point(double x, double y) :
        x(x), y(y) {}

    bool operator <(const Point &rhs) const {
        return (y < rhs.y) || ((y == rhs.y) && (x < rhs.x));
    }

    Point operator +(const Point &rhs) const {
        return {x + rhs.x, y + rhs.y};
    }

    Point operator -(const Point &rhs) const {
        return {x - rhs.x, y - rhs.y};
    }

    bool operator ==(const Point &rhs) const {
        return ((x == rhs.x) && (y == rhs.y));
    }

    void reverse() {
        x *= -1;
        y *= -1;
    }
};

bool counterClockWise(Point v1, Point v2) {
    double dot = v1.x * v2.x + v1.y * v2.y;
    double det = v1.x * v2.y - v1.y * v2.x;
    return (atan2(det, dot) > 0);
}

std::vector<Point> MinkowskiAddition(std::deque<Point> &v1, std::deque<Point> &v2) {
    int n = v1.size();
    int m = v2.size();
    v1.push_back(v1[0]);
    v1.push_back(v1[1]);
    v2.push_back(v2[0]);
    v2.push_back(v2[1]);
    std::vector<Point> result;
    int i = 0;
    int j = 0;
    while ((i < n) || (j < m)) {
        result.push_back(v1[i] + v2[j]);
        if (counterClockWise(v1[i + 1] - v1[i], v2[j + 1] - v2[j])) {
            ++i;
        } else {
            if (counterClockWise(v2[j + 1] - v2[j], v1[i + 1] -  v1[i])) {
                ++j;
            } else {
                ++i;
                ++j;
            }
        }
    }
    return result;
}

void readPolygon(std::deque<Point> &polygon, std::istream& is) {
    int n;
    is >> n;
    double x, y;
    for (int i = 0; i < n; ++i) {
        is >> x >> y;
        polygon.push_front({x, y});
    }
}

bool zeroInside(std::vector<Point> &polygon) {
    int n = polygon.size();
    Point zero(0, 0);
    for (int i = 0; i < n; ++i) {
        if (polygon[i] == zero) {
            return true;
        }
        if (counterClockWise(zero - polygon[i], polygon[(i + 1) % n] - polygon[i])) {
            return false;
        }
    }
    return true;
}

void setFirstPoint(std::deque<Point> &polygon) {
    int min_index = 0;
    for (int i = 1; i < polygon.size(); ++i) {
        if (polygon[i] < polygon[min_index])
            min_index = i;
    }
    for (int i = 0; i < min_index; ++i) {
        polygon.push_back(polygon[0]);
        polygon.pop_front();
    }
}

int main() {
    std::fstream fin("input.txt");
    std::deque<Point> p1, p2;
    readPolygon(p1, fin);
    readPolygon(p2, fin);
    for (auto& point : p2) {
        point.reverse();
    }
    setFirstPoint(p1);
    setFirstPoint(p2);
    auto addition = MinkowskiAddition(p1, p2);
    if (zeroInside(addition)) {
        std::cout << "YES";
    } else {
        std::cout << "NO";
    }
    return 0;
}

