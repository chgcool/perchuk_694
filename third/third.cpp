// Перчук Святослав
// B2. Строка по z-функции
// Найти лексикографически-минимальную строку, построенную по z-функции, в алфавите a-z.

#include <fstream>
#include <iostream>
#include <set>
#include <vector>

const char FIRST_SYMBOL = 'a';

char FindSuitableSymbol(const std::set<char>& symbols) {
    char last_symbol = FIRST_SYMBOL;
    for (auto symbol : symbols) {
        if (symbol - last_symbol > 1) {
            break;
        }
        last_symbol = symbol;
    }
    return static_cast<char>(last_symbol + 1);
}

std::vector<int> PrefixFunctionByZFunction (const std::vector<int> &z_function) {
    std::vector<int> prefix_function(z_function.size(), 0);
    for (int i = 1; i < z_function.size(); ++i) {
        for (int j = z_function[i]; j > 0; --j) {
            if (prefix_function[i + (j - 1)] != 0) {
                break;
            }
            prefix_function[i + (j - 1)] = j;
        }
    }
    return prefix_function;
}

std::string MinimalStringByPrefixFunction (const std::vector<int> &prefix_function) {
    if (prefix_function.empty()) {
        return "";
    }
    std::string result_string(1, FIRST_SYMBOL);
    for (auto i = 1; i < prefix_function.size(); ++i) {
        if (prefix_function[i] > 0) {
            result_string.push_back(result_string[prefix_function[i] - 1]);
        }
        else {
            std::set<char> busy_symbols = {FIRST_SYMBOL};
            int index_of_busy = prefix_function[i - 1];
            while (index_of_busy > 0) {
                busy_symbols.insert(result_string[index_of_busy]);
                index_of_busy = prefix_function[index_of_busy - 1];
            }
            char symbol = FindSuitableSymbol(busy_symbols);
            result_string += symbol;
        }
    }
    return result_string;
}

int main() {
    std::ifstream fin("input.txt");
    std::vector<int> z_function;
    int value;
    while(fin >> value) {
        z_function.push_back(value);
    }
    std::cout << MinimalStringByPrefixFunction(PrefixFunctionByZFunction(z_function));
    return 0;
};
