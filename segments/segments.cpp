//
// created by Svyatoslav
//
/* Контест 3, Задача 1 - Расстояние между отрезками
Даны два отрезка в пространстве (x1, y1, z1) - (x2, y2, z2) и (x3, y3, z3) - (x4, y4, z4).
Найдите расстояние между отрезками.
 */

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

using std::cout;
using std::make_pair;

double abs(double d) {
    return (d > 0 ? d : -d);
}

struct Vector {
    double x;
    double y;
    double z;

    Vector(double x, double y, double z) :
            x(x), y(y), z(z) {}

    Vector(std::vector<int> v) {
        if (v.size() < 3) {
            return;
        }
        x = v[0];
        y = v[1];
        z = v[2];
    }

    const Vector operator *(double d) {
        Vector result = *this;
        result.x *= d;
        result.y *= d;
        result.z *= d;
        return result;
    }
};

const Vector operator -(const Vector& lhs, const Vector& rhs) {
    return {lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z};
}

const Vector operator +(const Vector& lhs, const Vector& rhs) {
    return {lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z};
}

double scalar(const Vector& v1, const Vector& v2) {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

double length(const Vector &v) {
    return sqrt(scalar(v, v));
}

double segmentToSegmentDistance(const Vector& A, const Vector& B, const Vector& C, const Vector& D) {
    Vector u = B - A;
    Vector v = D - C;
    Vector w = A - C;
    double a = scalar(u, u);
    double b = scalar(u, v);
    double c = scalar(v, v);
    double d = scalar(u, w);
    double e = scalar(v, w);
    double E = a * c - b * b;
    double sc, sN, tc, tN;
    double sD = E;
    double tD = E;

    if (E == 0) {
        sN = 0.0;
        sD = 1.0;
        tN = e;
        tD = c;
    } else {
        sN = (b * e - c * d);
        tN = (a * e - b * d);
        if (sN <= 0.0) {
            sN = 0.0;
            tN = e;
            tD = c;
        } else {
            if (sN >= sD) {
                sN = sD;
                tN = e + b;
                tD = c;
            }
        }
    }

    if (tN <= 0.0) {
        tN = 0.0;
        if (-d < 0.0) {
            sN = 0.0;
        } else {
            if (-d > a) {
                sN = sD;
            }
            else {
                sN = -d;
                sD = a;
            }
        }
    } else {
        if (tN >= tD) {
            tN = tD;
            if ((-d + b) < 0.0) {
                sN = 0;
            } else {
                if ((-d + b) > a) {
                    sN = sD;
                } else {
                    sN = (-d +  b);
                    sD = a;
                }
            }
        }
    }
    sc = (abs(sN) == 0.0 ? 0.0 : sN / sD);
    tc = (abs(tN) == 0.0 ? 0.0 : tN / tD);
    Vector dP = w + (u * sc) - (v * tc);
    return length(dP);
}

int main() {
    std::vector<std::vector<int>> points(4, std::vector<int> (3));
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 3; ++j) {
            std::cin >> points[i][j];
        }
    }
    std::cout << std::fixed << std::setprecision(8);
    std::cout << segmentToSegmentDistance(points[0], points[1], points[2], points[3]);
    return 0;
}
