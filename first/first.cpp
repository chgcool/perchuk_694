// Перчук Святослав
// Задача 1. Поиск подстроки (5 баллов)
// Поик всех вхождений шаблона длины p в строку длины n за O(n + p) с
// использованием доп. памяти O(p) с помощью z-функции.

#include <deque>
#include <fstream>
#include <iostream>
#include <vector>

std::vector<int> calculateZFunction (std::string s) {
    // случай пустой строки
    if (!s.size()) {
        return std::vector<int> (0);
    }
    std::vector<int> z_array (s.size(), 0);
    z_array[0] = s.size();
    int left = 0;
    int right = 0;
    for (int i = 1; i < s.size(); ++i) {
        if (i <= right) {
            z_array[i] = std::min(right - i + 1, z_array[i - left]);
        }
        while ((i + z_array[i] < s.size()) && (s[i + z_array[i]] == s[z_array[i]])) {
            ++z_array[i];
        }
        if (i + z_array[i] - 1 > right) {
            left = i;
            right = i + z_array[i] - 1;
        }
    }
    return z_array;
}

int main() {
    //std::ios_base::sync_with_stdio(false);
    std::ifstream fin("input.txt");

    std::string pattern;
    //std::cin >> pattern;
    fin >> pattern;
    auto z_array = calculateZFunction(pattern);

    std::deque<char> buffer;
    char symbol;
    int index_in_text = pattern.size();
    int left = 0;
    int right = 0;
    const int patterns_in_buffer = 100;
    // будем считывать элементы в строку-буффер размера O(p), поэтапно применяя z-функцию,
    // убеждаясь, граница строки достаточно далеко.
    while (fin.get(symbol)) {
        buffer.push_back(symbol);
        if (buffer.size() == patterns_in_buffer * pattern.size()) {
            int buf_index = 0;
            // высчитываем функцию для элементов, точно находящихся достаточно далеко от границы строки.
            while (buf_index < (patterns_in_buffer - 1) * pattern.size()) {
                int z_i_value = 0;
                if (index_in_text <= right) {
                    z_i_value = std::min(z_array[index_in_text - left], right - index_in_text + 1);
                }
                while ((buf_index + z_i_value < patterns_in_buffer * pattern.size()) &&
                        (buffer[buf_index + z_i_value] == pattern[z_i_value])) {
                    ++z_i_value;
                }
                if (index_in_text + z_i_value - 1 > right) {
                    left = index_in_text;
                    right = index_in_text + z_i_value - 1;
                }
                if (z_i_value == pattern.size()) {
                    std::cout << index_in_text - pattern.size() - 1 << ' ';
                }
                ++index_in_text;
                ++buf_index;
            }
            // сохраним элементы, для которых z-функция еще не была посчитана.
            for (int waste = 0; waste < buf_index; ++waste) {
                buffer.pop_front();
            }
        }
    }
    // Обработаем оставшиеся элементы, теперь зная наверняка, где лежит граница строки.
    for (int buf_index = 0; buf_index < buffer.size(); ++buf_index, ++index_in_text) {
        int z_i_value = 0;
        if (index_in_text <= right) {
            z_i_value = std::min(z_array[index_in_text - left], right - index_in_text + 1);
        }
        while ((buf_index + z_i_value < buffer.size()) && (buffer[buf_index + z_i_value] == pattern[z_i_value])) {
            ++z_i_value;
        }
        if (index_in_text + z_i_value - 1 > right) {
            left = index_in_text;
            right = index_in_text + z_i_value - 1;
        }
        if (z_i_value == pattern.size()) {
            std::cout << index_in_text - pattern.size() - 1 << ' ';
        }
    }
    return 0;
}