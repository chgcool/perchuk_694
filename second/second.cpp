// Перчук Святослав
// B1. Строка по префикс функции
// Найти лексикографически-минимальную строку, построенную по префикс-функции, в алфавите a-z.

#include <fstream>
#include <iostream>
#include <set>
#include <vector>

const char FIRST_SYMBOL = 'a';

char FindSuitableSymbol(const std::set<char>& symbols) {
    char last_symbol = FIRST_SYMBOL;
    for (auto symbol : symbols) {
        if (symbol - last_symbol > 1) {
            break;
        }
        last_symbol = symbol;
    }
    return static_cast<char>(last_symbol + 1);
}
	

std::string BuildStringByPrefixFunction(const std::vector<int> &prefix_function) {
    if (prefix_function.empty()) {
        return "";
    }
    std::string result_string(1, FIRST_SYMBOL);
    for (auto i = 1; i < prefix_function.size(); ++i) {
        if (prefix_function[i] > 0) {
            result_string.push_back(result_string[prefix_function[i] - 1]);
        }
        else {
            std::set<char> unacceptable_symbols = {FIRST_SYMBOL};
            int index_of_unacceptable = prefix_function[i - 1];
            while (index_of_unacceptable > 0) {
                unacceptable_symbols.insert(result_string[index_of_unacceptable]);
                index_of_unacceptable = prefix_function[index_of_unacceptable - 1];
            }
            
            char symbol = FindSuitableSymbol(unacceptable_symbols);
            result_string += symbol;
        }
    }
    return result_string;
}

int main() {
    std::ifstream fin("input.txt");
    std::vector<int> prefix_function;
    int value;
    while (fin >> value) {
        prefix_function.push_back(value);
    }
    std::cout << BuildStringByPrefixFunction(prefix_function);
    return 0;
}

