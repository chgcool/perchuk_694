//
// created by Svyatoslav Perchuk
//
// C. Шаблон с ?
// Найти все позиции вхождения шаблона с '?' в строку за O(n + m + Z) алгоритмом Ахо-Корасика.

#include <fstream>
#include <iostream>
#include <memory>
#include <unordered_map>
#include <vector>

using std::make_shared;
using std::unique_ptr;

// Префиксное дерефо, строящееся по строке-шаблону, для поиска всех вхождений шаблона в любой заданный текст.
class PrefixTree {
public:
    // конструктор, строит префиксное дерево по шаблону
    explicit PrefixTree(const std::string&);

    // функция, находящая все вхожения
    std::vector<int> countOccurrences(const std::string&) ;

private:
    // структура-вершина дерева, хранит необходимую информацию о дочерних вершинах, суфф. ссылках и т.д.
    struct Node;
    using Link = Node*;
    const std::unique_ptr<Node> root; // корень дерева
    int length_of_pattern; // длина всего шаблона
    int number_of_subpatterns; // количество подстрок в шаблоне между знаками '?'

    // добавление строки в префиксное дерево
    void addString(const std::string&, int);
};

struct PrefixTree::Node {
    bool is_final; // завершается ли какая-нибудь подстрока в этой вершине
    char leading_symbol; // символ на ребре, ведущем в эту вершину, используется для получения суффиксной ссылки
    Link parent; 
    Link suffix_link;
    Link final_suffix_link; // оптимизированная суффиксная ссылка
    std::vector<int> final_positions; // номера подстрок шаблона, оканчивающихся в этой вершине
    std::unordered_map<char, unique_ptr<Node>> child_nodes; // фактические дочерние вершины
    std::unordered_map<char, Link> next_nodes; // вершины, в которые можно перейти по ссылке

    Node();

    Node(Link, char);

    // ссылка на дочернюю вершину по элементу с автоматическим созданием при отсутствии
    Link getChild(char);

    // переход в следующую вершину по символу
    Link getNextNode(char);

    // нахождение суффиксной ссылки
    Link getSuffixLink();

    // нахождение оптимизированной суффиксной ссылки
    Link getFinalSuffixLink();
};

PrefixTree::Node::Node():
        is_final(false),
        leading_symbol('0'),
        parent(this),
        suffix_link(this),
        final_suffix_link(nullptr) {}

PrefixTree::Node::Node(Link parent_, char leading_symbol_):
        is_final(false),
        leading_symbol(leading_symbol_),
        // Родитель корня - корень. Это используется при проверке, ведь только он является собственным родителем
        parent(parent_),
        suffix_link(nullptr),
        final_suffix_link(nullptr) {}

PrefixTree::Link PrefixTree::Node::getChild(char symbol) {
    if (child_nodes.count(symbol) == 0) {
        child_nodes[symbol].reset(new Node(this, symbol));
    }
    return child_nodes[symbol].get();
}

PrefixTree::Link PrefixTree::Node::getNextNode(char symbol) {
    if (next_nodes.count(symbol) == 0) {
        // если не можем перейти в дочернюю вершину, ищем суффиксную ссылку
        if (child_nodes.count(symbol) == 0) {
            if (getSuffixLink() == this) { // случай корня
                next_nodes[symbol] = this;
            } else {
                next_nodes[symbol] = getSuffixLink()->getNextNode(symbol);
            }
        } else {
            next_nodes[symbol] = child_nodes[symbol].get();
        }
    }
    return next_nodes[symbol];
}

PrefixTree::Link PrefixTree::Node::getSuffixLink() {
    if (suffix_link == nullptr) {
        if (parent->getSuffixLink() == parent) { // это корень или родитель корень
            suffix_link = parent;
        } else {
            suffix_link = (parent->suffix_link)->getNextNode(leading_symbol);
        }
    }
    return suffix_link;
}

PrefixTree::Link PrefixTree::Node::getFinalSuffixLink() {
    if (final_suffix_link == nullptr) {
        Link temp_link = getSuffixLink();
        if (temp_link->parent == parent) { // случай корня
            final_suffix_link = temp_link;
        } else {
            if(!temp_link->is_final) {
                temp_link = temp_link->getFinalSuffixLink();
            }
            final_suffix_link = temp_link;
        }
    }
    return final_suffix_link;
}

PrefixTree::PrefixTree(const std::string& pattern) :
        root(new Node()),
        length_of_pattern(pattern.size()),
        number_of_subpatterns(0)
{
    // начало и конец считываемого подшаблона
    size_t begin = 0;
    size_t end = 0;
    for (;end < pattern.size(); ++end) {
        if (pattern[end] == '?') {
            if (end >= begin) {
                addString(pattern.substr(begin, end - begin), end - 1);
            }
            begin = end + 1;
        }
    }
    addString(pattern.substr(begin, end - begin), end - 1);
}

std::vector<int> PrefixTree::countOccurrences(const std::string& text) {
    Link current_node = root.get();
    std::vector<int> occurrences(text.size(), 0);
    for (int i = 0; i < text.size(); ++i) {
        current_node = current_node->getNextNode(text[i]);
        for (auto suf_node = current_node; suf_node != root.get(); suf_node = suf_node->getFinalSuffixLink()) {
            if (suf_node->is_final) {
                for (auto position_in_pattern : suf_node->final_positions) {
                    int real_end = i + length_of_pattern - position_in_pattern - 1;
                    if (real_end < text.size()) {
                        ++occurrences[real_end];
                    }
                }
            }
        }
    }
    std::vector<int> result;
    for (int i = length_of_pattern - 1; i < occurrences.size(); ++i) {
        if (occurrences[i] == number_of_subpatterns) {
            result.push_back(i - length_of_pattern + 1);
        }
    }
    return result;
}

void PrefixTree::addString(const std::string& subpattern, int final_symbol_number) {
    if (subpattern.empty())
        return;
    Link current_node = root.get();
    for (auto symbol: subpattern) {
        current_node = current_node->getChild(symbol);
    }
    current_node->is_final = true;
    current_node->final_positions.push_back(final_symbol_number);
    ++number_of_subpatterns;
}

int main() {
    std::ifstream fin("input.txt");
    std::string pattern, text;
    fin >> pattern >> text;
    PrefixTree Karasik(pattern);
    for (auto i : Karasik.countOccurrences(text)) {
    	std::cout << i << ' ';
    }
    return 0;
}

